package com.baipiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaipiaoHappyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaipiaoHappyApplication.class, args);
    }

}
